*** Settings ***
Resource        resources/ComumptBR.robot
Resource        resources/PO/Install4ptBR.robot
Resource        resources/PO/LoginBackend4ptBR.robot
Resource        resources/PO/LoginFrontend4ptBR.robot
Test Setup      Abrir Navegador
Test Teardown   Fechar Navegador

*** Variables ***
${HOSTDB}  j3db
${NOMEBANCO}  bancoj3

*** Test Cases ***
Caso de Teste 01: Instalação do Joomla 4
    Abrir Pagina de Instalação
    Selecionar "pt-BR" na instalação
    Digitar o nome do site "Teste Joomla!" no campo do nome do site
    Digitar o nome real "Administrador de Teste" no campo nome do Super utilizador
    Digitar o nome do utilizador "usuario1" no campo de nome de utilizador
    Digitar a senha "senha1234567" nos campos de senha
    Digitar o email "teste@bop.tec.br" no campo do e-mail do administrador
    Clicar no botão próximo da página Configuração
    Digitar o nome do servidor "${HOSTDB}" no campo de nome do servidor
    Digitar o nome do utilizador "root" no campo de nome do usuario do DB
    Digitar a senha "example" no campo senha do banco de dados
    Digitar o nome do banco "${NOMEBANCO}" no campo nome do banco
    Clicar no botão próximo da página Banco de dados
    Instalar idioma pt-br

Caso de Teste 02: Login no Administrator Joomla 4
    Abrir Página Administrator do Joomla
    Login no Administrator do Joomla com "usuario1" e senha "senha1234567"
    Enviar estatisticas sempre
    Logout do Administrator

Caso de Teste 03: Login no Frontend Joomla 4
    Abrir Página Frontend do Joomla
    Login no Frontend do Joomla com "usuario1" e "senha1234567"
    Saudação para "Administrador de Teste"
    Logout do Frontend
