#!/bin/bash
set -x
chmod +x /tests/wait-for-it.sh

/tests/wait-for-it.sh j3:80
VARS='-v BROWSER:headlesschrome -v URL_INSTALL:http://j3/installation/index.php -v URL_ADMINISTRATOR:http://j3/administrator/index.php -v URL_FRONTEND:http://j3/index.php -v HOSTDB:j3db'
if test ! -d /results/JoomlaptBR; then
    mkdir -p /results/JoomlaptBR
fi
robot -d /results/JoomlaptBR $VARS  /tests/JoomlaptBR.robot

/tests/wait-for-it.sh j4:80
VARS='-v BROWSER:headlesschrome -v URL_INSTALL:http://j4/installation/index.php -v URL_ADMINISTRATOR:http://j4/administrator/index.php -v URL_FRONTEND:http://j4/index.php -v HOSTDB:j4db'
if test ! -d /results/Joomla4ptBR; then
    mkdir -p /results/Joomla4ptBR
fi
robot -d /results/Joomla4ptBR $VARS /tests/Joomla4ptBR.robot
