# Install Joomla

Run the Joomla installation using robot framework running on a docker image prepared for testing and using standard joomla images for web and database. (en)
Executa a instalação do Joomla usando robot framework executado em uma imagem docker preparada para o teste e usando as imagens padrão do Joomla! para web e banco de dados. (pt-br)

## Depends

- [Docker Run Chrome](https://gitlab.com/boptec/robot/runchrome)
- [Wait For It Shell Script](https://github.com/vishnubob/wait-for-it)
- [Joomla! Docker Images](https://hub.docker.com/_/joomla)

## Execução local

```bash
 install-joomla (main)
$ robot -d ./results/ -t "Caso de Teste 02: Login no Administrator" -v OPTIONS:"" -v URL:http://localhost:8080/administrator  InstallJoomlaptBR.robot
```
