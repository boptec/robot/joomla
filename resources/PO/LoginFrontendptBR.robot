*** Variables ***
${URL_FRONTEND}     http://joomla/index.php


*** Keywords ***
Abrir Página Frontend do Joomla
    Go To           ${URL_FRONTEND}
    Capture Page Screenshot

Login no Frontend do Joomla com "${USER_FRONTEND}" e "${USER_PASS}"
    Wait Until Element Is Visible   id=modlgn-username
    Capture Page Screenshot
    Input Text                      id=modlgn-username  ${USER_FRONTEND}
    Input Text                      id=modlgn-passwd    ${USER_PASS}
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="form-login-submit"]/div/button
    Wait Until Element Is Visible   xpath=//*[@id="login-form"]/div[1]
    Capture Page Screenshot

Logout do Frontend
    Click Element    xpath=//*[@id="login-form"]/div[2]/input[@value='Sair']
    Capture Page Screenshot
