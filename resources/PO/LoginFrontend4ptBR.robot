*** Variables ***
${URL_FRONTEND}     http://joomla/index.php


*** Keywords ***
Abrir Página Frontend do Joomla
    Go To           ${URL_FRONTEND}
    Capture Page Screenshot

Login no Frontend do Joomla com "${USER_FRONTEND}" e "${USER_PASS}"
    Wait Until Element Is Visible   id=modlgn-username-16
    Capture Page Screenshot
    Input Text                      id=modlgn-username-16  ${USER_FRONTEND}
    Input Text                      id=modlgn-passwd-16    ${USER_PASS}
    Capture Page Screenshot
    Click Element                   xpath=//*[@id="login-form-16"]/div/div[4]/button
    Wait Until Element Is Visible   xpath=//*[@id="login-form-16"]/div[1]
    Capture Page Screenshot

Saudação para "${ADMIN_REAL}"
    Element Should Contain          xpath=//*[@id="login-form-16"]/div[1]    ${ADMIN_REAL}

Logout do Frontend
    Click Element    xpath=//*[@id="login-form-16"]/div[2]/input[1]
    Capture Page Screenshot
